import datetime
import hashlib

import google.oauth2.credentials
import googleapiclient.discovery

import pytz
from . import utils


def getClassroomAssignmentDue(assignment, tz):
	if 'dueDate' in assignment: # has a due date

		dueOnDateOnly = 'dueDate' in assignment and not 'dueTime' in assignment

		# Get timezone
		local_timezone = pytz.timezone(tz)

		if not dueOnDateOnly:
			hour = str(assignment['dueTime']['hours']) if 'hours' in assignment['dueTime'] else '00'
			minute = str(assignment['dueTime']['minutes']) if 'minutes' in assignment['dueTime'] else '00'
			second = str(assignment['dueTime']['seconds']) if 'seconds' in assignment['dueTime'] else '00'
		else:
			hour, minute, second = '00', '00', '00'

		due = datetime.datetime.strptime(str(assignment['dueDate']['year']) + '-' + str(assignment['dueDate']['month']) + '-' + str(assignment['dueDate']['day']) + ' ' + hour + ':' + minute + ':' + second, '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)

		# find difference
		timeleft = due - datetime.datetime.now(datetime.timezone.utc)

		if timeleft >= datetime.timedelta(days=1):
			status = 2 # not due yet
		elif timeleft > datetime.timedelta(days=0):
			status = 1 # due in 24h
		else:
			status = 0 # late

		# Calculate time left in seconds
		timeleft_seconds = timeleft.total_seconds()
		timeleft = str(abs(timeleft)).rsplit(":", 1)[0]

		# Translate into user's timezone and strftime
		due = due.astimezone(local_timezone).strftime('%b %d, %Y %I:%M %p')
		return due, timeleft, timeleft_seconds, status
	else: # doesn't have a due date
		return None, None, None, 2

def getGoogleClasses(creds):
	credentials = google.oauth2.credentials.Credentials(**creds)
	classroom = googleapiclient.discovery.build('classroom', 'v1', credentials=credentials)

	classes = {}

	try:
		googleClasses = classroom.courses().list().execute()['courses']
	except:
		googleClasses={}

	for googleClass in googleClasses:
		if googleClass['courseState'] == 'ACTIVE':
			classes[googleClass['id']] = {}
			classes[googleClass['id']]['name'] = googleClass['name']
			try:
				classes[googleClass['id']]['description'] = googleClass['description']
			except:
				classes[googleClass['id']]['description'] = "No description"
			classes[googleClass['id']]['platform'] = "Classroom"
			if googleClass['courseState'] == "ACTIVE":
				classes[googleClass['id']]['active'] = True
			else:
				classes[googleClass['id']]['active'] = False
			classes[googleClass['id']]['link'] = googleClass['alternateLink']
			classes[googleClass['id']]['color'] = hashlib.md5(googleClass['name'].encode('utf-8')).hexdigest()[:6]

	numClasses = len(classes)
	return classes, numClasses


def getAssignmentDoneState(submission_object, courseId, courseWorkId):

	done = submission_object.list(courseId=courseId, courseWorkId=courseWorkId).execute()["studentSubmissions"][0]
	return done


def getClassroomAssignments(creds, assignments_per_page, scope="all", page_number=1):
	credentials = google.oauth2.credentials.Credentials(**creds)
	classroom = googleapiclient.discovery.build('classroom', 'v1', credentials=credentials)

	userConfig = utils.getUserConfig(creds)

	assignments = {}
	if scope == 'all':
		try:
			googleClasses = classroom.courses().list().execute()['courses']
		except:
			googleClasses={}

		classwork = []

		for googleClass in googleClasses:
			if 'courseState' in googleClass:
				if googleClass['courseState'] == 'ACTIVE':
					# Only show active classes
					workForClass = classroom.courses().courseWork()
					submission = workForClass.studentSubmissions()
					workForClass = workForClass.list(courseId=googleClass['id']).execute()
					if 'courseWork' in workForClass:
						for work in workForClass["courseWork"]:
							work["className"] = googleClass["name"]
						classwork.extend(workForClass['courseWork'])
	else:
		googleClass = classroom.courses().get(id=scope).execute()
		classwork = classroom.courses().courseWork()
		submission = classwork.studentSubmissions()
		classwork = classwork.list(courseId=scope).execute()
		classwork = classwork["courseWork"]
		for work in classwork:
			work["className"] = googleClass["name"]

	# limit to range if not zero
	totalNumAssignments = len(classwork)
	if page_number != 0:
		last_assignment_number = page_number * assignments_per_page
		classwork = classwork[last_assignment_number - assignments_per_page:last_assignment_number]

	for work in classwork:
		assignments[work['id']] = {}
		assignments[work['id']]['name'] = work['title']
		if 'description' in work:
			assignments[work['id']]['description'] = work['description']
		else:
			assignments[work['id']]['description'] = ''
		due, timeleft, timeleft_seconds, status = getClassroomAssignmentDue(work, userConfig['system']['timezone'])

		assignments[work['id']]['due'] = due
		assignments[work['id']]['timeleft'] = timeleft
		assignments[work['id']]['timeleft_seconds'] = timeleft_seconds
		assignments[work['id']]['status'] = status
		assignments[work['id']]['class'] = work["className"]

		# retrieve more assignment metadata
		done = getAssignmentDoneState(submission, work['courseId'], work['id'])

		assignments[work['id']]['done'] = (done['state'] in ["TURNED_IN", "RETURNED"]) or ('assignedGrade' in done)
		assignments[work['id']]['course_id'] = done['courseId']
		assignments[work['id']]['id'] = done['id'] # id of student submission


		assignments[work['id']]['coursework_id'] = work['id']
		assignments[work['id']]['link'] = work['alternateLink']
		assignments[work['id']]['platform'] = 'Classroom'

		assignments[work['id']]['local_link'] = f'/classes/{done["courseId"]}/assignment/{work["id"]}/{done["id"]}/'

	assignments = dict(
		sorted(
			assignments.items(),
			key = lambda tup: (
				tup[1]['timeleft_seconds'] is None,
				tup[1]["done"],
				tup[1]["timeleft_seconds"] if tup[1]['timeleft_seconds'] is not None else -1000000000000000
			)
		)
	)

	numAssignments = len(assignments)

	return assignments, numAssignments, totalNumAssignments


def getAssignmentDetails(creds, course_id, coursework_id, id):
	credentials = google.oauth2.credentials.Credentials(**creds)
	classroom = googleapiclient.discovery.build('classroom', 'v1', credentials=credentials)
	userConfig = utils.getUserConfig(creds)

	primary_details = classroom.courses().courseWork().get(courseId=course_id, id=coursework_id).execute()

	secondary_details = classroom.courses().courseWork().studentSubmissions().get(courseId=course_id, courseWorkId=coursework_id, id=id).execute()

	for key in list(primary_details.keys()): # primary takes precedence over secondary
		secondary_details[key] = primary_details[key]

	due, timeleft, timeleft_seconds, status = getClassroomAssignmentDue(secondary_details, userConfig['system']['timezone'])

	secondary_details['due'] = due
	secondary_details['timeleft'] = timeleft
	secondary_details['timeleft_seconds'] = timeleft_seconds
	secondary_details['status'] = status
	secondary_details['done'] = (secondary_details['state'] in ["TURNED_IN", "RETURNED"]) or ('assignedGrade' in secondary_details)

	return secondary_details

