import io
from configparser import ConfigParser
from datetime import datetime

import google.oauth2.credentials
import google.auth.transport.requests
import googleapiclient.discovery
from googleapiclient.http import MediaIoBaseUpload, MediaIoBaseDownload

import pytz


def makeDefaultConfig():
	"""Construct a default config file.
	Used for new users and updating existing configs
	"""

	config = ConfigParser()
	config.add_section('appearance')
	config['appearance']['theme'] = 'light'
	config['appearance']['darkThemeTimerStart'] = '20:00'
	config['appearance']['darkThemeTimerEnd'] = '06:00'

	config.add_section('system')
	config['system']['timezone'] = 'UTC'

	return config

def mergeUserConfig(userConfig):
	"""Merge existing user config with default config from makeDefaultConfig
	"""

	new_config = makeDefaultConfig()

	oldConfig = []

	for each_section in userConfig.sections():
		for (each_key, each_val) in userConfig.items(each_section):
			oldConfig.append((each_section, each_key, each_val))

	for i in range(len(oldConfig)):
		if not new_config.has_section(oldConfig[i][0]):
			new_config.add_section(oldConfig[i][0])
		new_config[oldConfig[i][0]][oldConfig[i][1]] = oldConfig[i][2]

	return new_config


def createUserConfig(creds):
	"""Upload default config to new users Google Drive.
	Returns a tuple of True (file uploaded successfully)
	or False (file exists already; not uploaded) and the file ID
	"""

	credentials = google.oauth2.credentials.Credentials(**creds)
	drive = googleapiclient.discovery.build('drive', 'v3', credentials=credentials)
	response = drive.files().list(spaces='appDataFolder', fields='nextPageToken, files(id, name)', pageSize=10).execute()
	files = list(filter(lambda item: item['name'] == 'config.ini', response.get('files', [])))
	if len(files) == 0:
		# Create config file
		config = makeDefaultConfig()
		f = io.StringIO()
		config.write(f)
		f.seek(0)
		f = io.BytesIO(f.read().encode('UTF-8'))
		file_metadata = {
			'name': 'config.ini',
			'parents': ['appDataFolder']
		}
		media = MediaIoBaseUpload(f, mimetype='text/plain', chunksize=1024*1024, resumable=True)
		file = drive.files().create(body=file_metadata, media_body=media, fields='id').execute()
		return True, file['id']
	return False, files[0]['id']

def deleteFile(service, filename):
	"""Delete filename from users Google Drive
	"""

	response = service.files().list(spaces='appDataFolder', fields='nextPageToken, files(id, name)', pageSize=10).execute()
	for file in response.get('files', []):
		if file.get('name') == filename:
			service.files().delete(fileId=file.get('id')).execute()

def getUserConfig(creds):
	"""Download user's config from their Google Drive
	Returns a ConfigParser object
	"""

	# Returns user config and creates it if it doesn't exist
	credentials = google.oauth2.credentials.Credentials(**creds)
	drive = googleapiclient.discovery.build('drive', 'v3', credentials=credentials)
	fileId = createUserConfig(creds)[1]
	request = drive.files().get_media(fileId=fileId)
	fh = io.BytesIO()
	downloader = MediaIoBaseDownload(fh, request)
	done = False
	while done is False:
		status, done = downloader.next_chunk()
	fh.seek(0)
	fh = io.StringIO(fh.read().decode('UTF-8'))
	config = ConfigParser()
	config.readfp(fh)
	config = mergeUserConfig(config)
	return config

def editUserConfig(creds, config):
	"""Update user's config in their Google Drive
	Overwrites existing config
	"""

	# Edit user config to given configparser object
	config = mergeUserConfig(config)
	credentials = google.oauth2.credentials.Credentials(**creds)
	drive = googleapiclient.discovery.build('drive', 'v3', credentials=credentials)
	deleteFile(drive, 'config.ini')
	f = io.StringIO()
	config.write(f)
	f.seek(0)
	f = io.BytesIO(f.read().encode('UTF-8'))
	file_metadata = {
		'name': 'config.ini',
		'parents': ['appDataFolder']
	}
	media = MediaIoBaseUpload(f, mimetype='text/plain', chunksize=1024*1024, resumable=True)
	file = drive.files().create(body=file_metadata, media_body=media, fields='id').execute()
	return True

def makeFile(creds, filename, contents, mimetype):
	"""Upload file to users Google Drive storage
	"""

	credentials = google.oauth2.credentials.Credentials(**creds)
	drive = googleapiclient.discovery.build('drive', 'v3', credentials=credentials)

	# Delete old file
	deleteFile(drive, filename)

	# Create new file
	f = io.StringIO()
	f.write(contents)
	f.seek(0)
	f = io.BytesIO(f.read().encode('UTF-8'))
	file_metadata = {
		'name': filename,
		'parents': ['appDataFolder']
	}
	media = MediaIoBaseUpload(f, mimetype=mimetype, chunksize=1024*1024, resumable=True)
	file = drive.files().create(body=file_metadata, media_body=media, fields='id').execute()

def getFile(creds, filename):
	"""Retrieve file from users Google Drive storage
	"""

	credentials = google.oauth2.credentials.Credentials(**creds)
	drive = googleapiclient.discovery.build('drive', 'v3', credentials=credentials)
	response = drive.files().list(spaces='appDataFolder', fields='nextPageToken, files(id, name)', pageSize=10).execute()
	files = list(filter(lambda item: item['name'] == filename, response.get('files', [])))
	if len(files) == 0:
		return None
	else:
		request = drive.files().get_media(fileId=files[0]['id'])
		fh = io.BytesIO()
		downloader = MediaIoBaseDownload(fh, request)
		done = False
		while done is False:
			status, done = downloader.next_chunk()
		fh.seek(0)
		fh = io.StringIO(fh.read().decode('UTF-8'))
		return fh.read()

def validateCredentials(session):
	"""Check if current session credentials are valid.
	Refreshes credentials if possible
	"""

	creds = None
	if 'credentials' in session:
		creds = google.oauth2.credentials.Credentials(**session['credentials'])

	if not creds or not creds.valid:
		if creds and creds.expired and creds.refresh_token:
			creds.refresh(google.auth.transport.requests.Request())
		else:
			return False

	# Credentials will have been validated
	return creds

def isTimeBetween(begin_time, end_time, time_zone):
	"""Check if current time in timezone is between begin_time and end_time
	"""

	tz = pytz.timezone(time_zone)
	check_time = datetime.now(tz).replace(tzinfo=None)
	check_time = datetime.strptime(datetime.strftime(check_time, '%H:%M'), '%H:%M') # strip off everything but hours and minutes

	if isinstance(begin_time, str):
		begin_time = datetime.strptime(begin_time, '%H:%M')
	if isinstance(end_time, str):
		end_time = datetime.strptime(end_time, '%H:%M')

	if begin_time < end_time:
		return check_time >= begin_time and check_time <= end_time
	else: # crosses midnight
		return check_time >= begin_time or check_time <= end_time

def getUserTheme(userConfig):
	"""Retrieve theme from userConfig.
	"""

	theme = 'light'
	if userConfig['appearance']['theme'] == 'dark':
		theme = 'dark'
	elif userConfig['appearance']['theme'] == 'custom':
		# Calculate if dark theme should be on
		begin_time = userConfig['appearance']['darkThemeTimerStart']
		end_time = userConfig['appearance']['darkThemeTimerEnd']
		timezone = userConfig['system']['timezone']

		if isTimeBetween(begin_time, end_time, timezone):
			# Dark theme should be enabled
			theme = 'dark'

	# If theme is unknown or light
	# If custom time range and time is not within range
	return theme
