for file in $(find templates/ -iname "*.html")
do
	js-beautify -t -r --type html -f "$file"
done