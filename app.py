import os
import math
from datetime import datetime
from urllib.parse import urlparse
import enum

from flask import (
	Flask,
	request,
	session,
	redirect,
	url_for,
	render_template,
	escape
)
from flask_sitemap import Sitemap
import google_auth_oauthlib.flow
import requests
import pytz

from services import googleclassroom, utils

# For local testing
# os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'


app = Flask(__name__, static_url_path='', static_folder='static')
app.config['SITEMAP_INCLUDE_RULES_WITHOUT_PARAMS'] = True
sitemap = Sitemap(app=app)

ASSIGNMENTS_PER_PAGE = 10

with open("secure/app_key.txt", "rb") as f:
	app.secret_key = f.read()

class AttachmentType(enum.Enum):
	DRIVEFILE = 'driveFile'
	YOUTUBE = 'youTubeVideo'
	LINK = 'link'
	FORM = 'form'

class Attachment:
	def __init__(self, attachmentdict):
		self.attachment_type = AttachmentType(next(iter(attachmentdict)))
		self.thumbnail = attachmentdict[self.attachment_type.value].get('thumbnailUrl', '')
		self.title = attachmentdict[self.attachment_type.value].get('title', '')

		url_types = {'alternateLink', 'url', 'formUrl'}
		key_intersection = list(set(attachmentdict[self.attachment_type.value]) & url_types)
		if key_intersection:
			self.url = attachmentdict[self.attachment_type.value][key_intersection[0]]
		else:
			self.url = '#'

	def __repr__(self):
		return f'Attachment<attachment_type={repr(self.attachment_type)}, thumbnail={repr(self.thumbnail)}, title={repr(self.title)}, url={repr(self.url)}>'

OAUTH_SCOPES = [
	'https://www.googleapis.com/auth/classroom.courses.readonly',
	'https://www.googleapis.com/auth/classroom.course-work.readonly',
	'https://www.googleapis.com/auth/classroom.coursework.me',
	'https://www.googleapis.com/auth/classroom.student-submissions.me.readonly',
	'https://www.googleapis.com/auth/classroom.announcements.readonly',
	'https://www.googleapis.com/auth/classroom.courseworkmaterials.readonly',
	'https://www.googleapis.com/auth/classroom.topics.readonly',
	'https://www.googleapis.com/auth/drive.appdata',
	'https://www.googleapis.com/auth/userinfo.profile'
]

def requires_auth(function):
	def wrapper(*args, **kwargs):
		valid = utils.validateCredentials(session)
		if valid is False:
			return redirect(url_for('googleOAuth', referrer=request.full_path))
		else:
			session['credentials'] = credentials_to_dict(valid)

		return function(*args, **kwargs)

	wrapper.__name__ = function.__name__
	return wrapper


def buildUser(credentials, addAssignments=True, addClasses=True, course_id='all', page_number=1):
	user = {}
	# Construct name and picture
	userdata = requests.get('https://www.googleapis.com/oauth2/v2/userinfo', headers={'Authorization': 'Bearer ' + session['credentials']['token']}).json()
	if 'name' not in userdata:
		return None
	user['name'] = userdata['name']
	user['picture'] = userdata['picture']

	# Construct assignments
	if addAssignments:
		assignments, numAssignments, totalNumAssignments = googleclassroom.getClassroomAssignments(credentials, ASSIGNMENTS_PER_PAGE, scope=course_id, page_number=page_number)
		user['assignments'] = assignments
		user['numAssignments'] = numAssignments
		user['totalNumAssignments'] = totalNumAssignments

	# Construct classes
	if addClasses:
		classes, numClasses = googleclassroom.getGoogleClasses(credentials)
		user['classes'] = classes
		user['numClasses'] = numClasses

	return user


@app.route("/")
def hello():
	if 'credentials' in session:
		user = buildUser(session['credentials'])
		if user is None:
			return render_template('landing.html', theme='light')
		userConfig = utils.getUserConfig(session['credentials'])

		return render_template('landing.html', user=user, theme=utils.getUserTheme(userConfig))
	else:
		return render_template('landing.html', theme='light')


@app.route("/assignments/")
@app.route("/assignments/page/<int:page_number>")
@requires_auth
def assignmentsPage(page_number=None):
	if page_number is None:
		page_number = 1
	user = buildUser(session['credentials'], addClasses=False, page_number=page_number)

	userConfig = utils.getUserConfig(session['credentials'])
	# assignments={'assignment1': {'name': 'Solve world hunger', 'description': 'Please solve world hunger by tuesday', 'due': time.time()+20000, 'class': 'Biowarfare II with Angela Stewart', 'done': False, 'link': 'https://www.abunchofcats.tk', 'platform':'Classroom'}, 'assignment2': {'name': 'Grow your own polyp', 'description': 'Grow your own polyp in the grower from your textbook', 'due': time.time()-1000, 'class': 'Medical Science 3.2 with Mr. Patty', 'done': False, 'link': 'https://www.abunchofcats.tk', 'platform':'TurnItIn'}, 'assignment3': {'name': 'Find the secret', 'description': 'Do it', 'due': time.time()+99999999, 'class': 'Intro to [redacted]', 'done': False, 'link': 'https://www.abunchofcats.tk', 'platform': 'Canvas'}, 'assignment4': {'name': "HEHE YOU'VE ALREADY DONE IT AND YOU DON'T EVEN KNOW WHAT IT IS YOU ABSOLUETE FOOL", 'description': 'HHOOO EHHEE', 'due': time.time()-32456, 'class': 'Head washing club', 'done': True, 'link': 'https://www.abunchofcats.tk', 'platform': 'Classroom'}}
	return render_template("assignments.html", assignments=user['assignments'], user=user, theme=utils.getUserTheme(userConfig), page_number=page_number, total_page_num=int(math.ceil(user['totalNumAssignments'] / ASSIGNMENTS_PER_PAGE)))


@app.route("/classes/")
@app.route("/classes/<course_id>/")
@app.route("/classes/<course_id>/page/<int:page_number>")
@app.route("/classes/<course_id>/assignment/<coursework_id>/<id>/")
@requires_auth
def classesPage(course_id=None, coursework_id=None, id=None, page_number=None):

	if course_id is None and coursework_id is None and id is None:
		# Viewing all classes
		user = buildUser(session['credentials'])

		userConfig = utils.getUserConfig(session['credentials'])
		return render_template("classes.html", user=user, theme=utils.getUserTheme(userConfig))

	elif course_id is not None and coursework_id is None and id is None:
		# Viewing single class assignments
		if page_number is None:
			page_number = 1

		user = buildUser(session['credentials'], course_id=course_id, page_number=page_number)
		userConfig = utils.getUserConfig(session['credentials'])
		return render_template("assignments.html", assignments=user['assignments'], user=user, theme=utils.getUserTheme(userConfig), page_number=page_number, total_page_num=int(math.ceil(user['totalNumAssignments'] / ASSIGNMENTS_PER_PAGE)))

	elif course_id is not None and coursework_id is not None and id is not None:
		# Viewing specific assignment
		assignment_details = googleclassroom.getAssignmentDetails(session['credentials'], course_id, coursework_id, id)

		user = buildUser(session['credentials'])
		userConfig = utils.getUserConfig(session['credentials'])

		try:
			attachments = [Attachment(attachment) for attachment in assignment_details['assignmentSubmission']['attachments']]
		except KeyError:
			# TODO: support short and long answer thingies
			attachments = []

		try:
			materials = [Attachment(material) for material in assignment_details['materials']]
		except KeyError:
			materials = []

		return render_template('view-assignment.html', assignment_details=assignment_details, attachments=attachments, materials=materials, user=user, theme=utils.getUserTheme(userConfig))


@app.route('/settings/')
@requires_auth
def settings():

	user = buildUser(session['credentials'], addClasses=False)

	userConfig = utils.getUserConfig(session['credentials'])

	# Create list of timezones

	zones = []
	for tz in pytz.common_timezones:
		offset = datetime.now(pytz.timezone(tz)).strftime('%z')
		zones.append('{0} (GMT{1}:{2})'.format(tz, offset[:-2], offset[-2:]))

	zones = sorted(zones, key=lambda zone: int(zone.split('GMT')[-1][:-1].replace(':', '')))

	return render_template('settings.html', userConfig=userConfig, theme=utils.getUserTheme(userConfig), errorMsg=request.args.get('errorMsg'), user=user, timezone_list=zones)


@app.route("/login/")
def googleOAuth():
	if request.referrer is not None:
		url = urlparse(request.referrer)
		url = f'{url.path}{"?" + url.query if url.query else ""}'
	else:
		url = None
	session['login_referrer'] = url or request.args.get('referrer') or '/assignments'

	flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
		'secure/google_oauth.json',
		scopes=OAUTH_SCOPES
	)

	flow.redirect_uri = 'https://academiic.azurewebsites.net/googleoauthcallback'
	# flow.redirect_uri = 'http://127.0.0.1:5000/googleoauthcallback'
	authorization_url, state = flow.authorization_url(access_type='offline', include_granted_scopes='true')
	session['state'] = state
	return redirect(authorization_url)


@app.route('/googleoauthcallback/')
def googleOAuthCallback():
	state = session['state']
	flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
		'secure/google_oauth.json',
		scopes=OAUTH_SCOPES,
		state=state
	)

	flow.redirect_uri = 'https://academiic.azurewebsites.net/googleoauthcallback'
	# flow.redirect_uri = 'http://127.0.0.1:5000/googleoauthcallback'
	authorization_response = request.url
	authorization_response = authorization_response.replace("http:", "https:")
	flow.fetch_token(authorization_response=authorization_response)
	credentials = flow.credentials
	session['credentials'] = credentials_to_dict(credentials)

	if 'login_referrer' in session:
		referrer = session.pop('login_referrer')
	else:
		referrer = '/assignments'

	return redirect(url_for('pageLoader', page=referrer, updateTimezone='true'))


@app.route('/loadpage/')
def pageLoader():
	if 'credentials' in session:
		user = buildUser(session['credentials'], addClasses=False)
		userConfig = utils.getUserConfig(session['credentials'])

		return render_template('loading.html', user=user, theme=utils.getUserTheme(userConfig))

	return render_template('loading.html')


@app.route('/updatepreferences/', methods=['POST'])
@requires_auth
def updatePreferences():
	userConfig = utils.getUserConfig(session['credentials'])

	if 'themeSelection' in request.form:
		theme = request.form['themeSelection']

		if theme not in ['light', 'dark', 'custom']:
			return redirect(url_for('settings', errorMsg='Invalid Theme Name: ' + escape(theme)))

		userConfig['appearance']['theme'] = theme

		if theme == 'custom':
			# Custom dark theme time
			startTime = request.form['start-time']
			endTime = request.form['end-time']

			# Accept blank times; reset to default
			if startTime == '':
				startTime = '20:00'
			if endTime == '':
				endTime = '06:00'

			# Validate start and end times
			try:
				datetime.strptime(startTime, '%H:%M')
			except ValueError:
				return redirect(url_for('settings', errorMsg='Invalid Start Time: ' + escape(startTime)))

			try:
				datetime.strptime(endTime, '%H:%M')
			except ValueError:
				return redirect(url_for('settings', errorMsg='Invalid End Time: ' + escape(endTime)))

			# strftime into config
			userConfig['appearance']['darkThemeTimerStart'] = startTime
			userConfig['appearance']['darkThemeTimerEnd'] = endTime



	if 'timeZone' in request.form:
		tz = request.form['timeZone']

		if tz not in pytz.common_timezones:
			return redirect(url_for('settings', errorMsg='Invalid Timezone: ' + escape(tz)))

		userConfig['system']['timezone'] = tz

	utils.editUserConfig(session['credentials'], userConfig)

	return redirect('/settings')


@app.route('/logout/')
@requires_auth
def logout():
	session.pop('credentials')
	return redirect('/')


@app.route("/revokeaccount/")
@requires_auth
def revokeAccount():
	if requests.post('https://oauth2.googleapis.com/revoke', params={'token': session['credentials']['token']}, headers={'content-type': 'application/x-www-form-urlencoded'}).status_code == 200:
		session.pop('credentials')
	return redirect('/')


@app.route('/privacypolicy/')
def privacy_policy():
	if 'credentials' in session:
		user = buildUser(session['credentials'])
		if user is None:
			return render_template('privacy_policy.html', theme='light')
		userConfig = utils.getUserConfig(session['credentials'])

		return render_template('privacy_policy.html', user=user, theme=utils.getUserTheme(userConfig))
	else:
		return render_template('privacy_policy.html', theme='light')


def credentials_to_dict(credentials):
	return {
		'token': credentials.token,
		'refresh_token': credentials.refresh_token,
		'token_uri': credentials.token_uri,
		'client_id': credentials.client_id,
		'client_secret': credentials.client_secret,
		'scopes': credentials.scopes
	}


@app.errorhandler(400)
def bad_request_err(e):
	print(f'Exception: {e}')
	try:
		if 'credentials' in session:
			userConfig = utils.getUserConfig(session['credentials'])
			return render_template('errors/400.html', theme=utils.getUserTheme(userConfig)), 400
		else:
			return render_template('errors/400.html', theme='light'), 400
	except Exception:
		return render_template('errors/400.html', theme='light'), 400


@app.errorhandler(401)
def unauthorized_err(e):
	print(f'Exception: {e}')
	try:
		if 'credentials' in session:
			userConfig = utils.getUserConfig(session['credentials'])
			return render_template('errors/401.html', theme=utils.getUserTheme(userConfig)), 401
		else:
			return render_template('errors/401.html', theme='light'), 401
	except Exception:
		return render_template('errors/401.html', theme='light'), 401


@app.errorhandler(403)
def forbidden_err(e):
	print(f'Exception: {e}')
	try:
		if 'credentials' in session:
			userConfig = utils.getUserConfig(session['credentials'])
			return render_template('errors/403.html', theme=utils.getUserTheme(userConfig)), 403
		else:
			return render_template('errors/403.html', theme='light'), 403
	except Exception:
		return render_template('errors/403.html', theme='light'), 403


@app.errorhandler(404)
def page_not_found_err(e):
	print(f'Exception: {e}')
	try:
		if 'credentials' in session:
			userConfig = utils.getUserConfig(session['credentials'])
			return render_template('errors/404.html', page=request.path, theme=utils.getUserTheme(userConfig)), 404
		else:
			return render_template('errors/404.html', page=request.path, theme='light'), 404
	except Exception:
		return render_template('errors/404.html', page=request.path, theme='light'), 404


@app.errorhandler(405)
def method_not_allowed_err(e):
	print(f'Exception: {e}')
	try:
		if 'credentials' in session:
			userConfig = utils.getUserConfig(session['credentials'])
			return render_template('errors/405.html', theme=utils.getUserTheme(userConfig)), 405
		else:
			return render_template('errors/405.html', theme='light'), 405
	except Exception:
		return render_template('errors/405.html', theme='light'), 405


@app.errorhandler(418)
def im_a_teapot_err(e):
	print(f'Exception: {e}')
	try:
		if 'credentials' in session:
			userConfig = utils.getUserConfig(session['credentials'])
			return render_template('errors/418.html', theme=utils.getUserTheme(userConfig)), 418
		else:
			return render_template('errors/418.html', theme='light'), 418
	except Exception:
		return render_template('errors/418.html', theme='light'), 418


@app.errorhandler(500)
def internal_server_error_err(e):
	print(f'Exception: {e}')
	try:
		if 'credentials' in session:
			userConfig = utils.getUserConfig(session['credentials'])
			return render_template('errors/500.html', theme=utils.getUserTheme(userConfig)), 500
		else:
			return render_template('errors/500.html', theme='light'), 500
	except Exception:
		return render_template('errors/500.html', theme='light'), 500
